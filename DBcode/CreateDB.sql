CREATE TABLE login (
    username TEXT NOT NULL PRIMARY KEY,
    password TEXT NOT NULL
);

CREATE TABLE users (
    name TEXT,
    phone TEXT UNIQUE CHECK (LENGTH(phone)>=9),
    email TEXT PRIMARY KEY,
    CONSTRAINT userr_ukey UNIQUE (username)
) INHERITS (login);

CREATE TABLE Doctor (
    Clinic TEXT,
    location TEXT,
    ID INTEGER PRIMARY KEY,
    gender TEXT CHECK (gender in ('Male','Female','Other')),
    Speciality TEXT,
    birthday DATE,
    CONSTRAINT Docter_ukey UNIQUE (username)
) INHERITS (users);

CREATE TABLE Patient (
    Address TEXT,
    Health_number INTEGER PRIMARY KEY,
    gender TEXT CHECK (gender in ('Male','Female','Other')),
    birthday DATE,
    CONSTRAINT Patient_ukey UNIQUE (username)
) INHERITS (users);

CREATE TABLE Pharmacy(
    Address TEXT,
    ID INTEGER PRIMARY KEY,
    CONSTRAINT Pharmacy_ukey UNIQUE (username),
    CONSTRAINT Pharmacy_ukey2 UNIQUE (email)
) INHERITS (users);

CREATE TABLE Appointment (
    Code SERIAL PRIMARY KEY,
    Location TEXT,
    Clinic TEXT,
    Speciality TEXT,
    Date DATE,
    Time TEXT,
    doctor INTEGER REFERENCES Doctor(ID) ON DELETE CASCADE,
    patient INTEGER REFERENCES Patient(Health_number) ON DELETE CASCADE
);

CREATE TABLE Medicine(
    Code SERIAL PRIMARY KEY,
    Name TEXT UNIQUE,
    Act_Substance TEXT
);

CREATE TABLE Prescription (
    Code SERIAL PRIMARY KEY,
    Dose TEXT,
    Duration TEXT,
    Quantity INTEGER,
    ExpirationDate DATE,
    Action TEXT,
    comment TEXT,
    patient INTEGER REFERENCES Patient(Health_number) ON DELETE CASCADE,
    doctor INTEGER REFERENCES Doctor(ID) ON DELETE CASCADE,
    medicine INTEGER REFERENCES Medicine(Code) ON DELETE CASCADE
);