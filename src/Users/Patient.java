package Users;

import java.time.LocalDate;
import java.util.Date;

public class Patient extends User {
    private String Address, Gender;
    private Integer Health_Number;
    private LocalDate Birth_date;
    private String Patient_name;


    public Patient(String Address, Integer Health_Number, String Gender, LocalDate birth) {
        /*
         * This is the one of Patient constructor, that contains the patient information
         * @param Address - String : Patient's Address
         * @param Health_Number - int : health number, which is unique
         * @param Gender - String : Patients gender
         * @param birth - String : patient's birthdate
         */

        this.Address = Address;
        this.Health_Number = Health_Number;
        this.Gender = Gender;
        this.Birth_date = birth;
    }

    public Patient(int hnum, String Name, String Phone, String email, String Address, LocalDate birthday){
        /*
         * This is the second Patient constructor, that contains all the patient information
         * @param hnum - int : health number, which is unique
         * @param Name - String : Patient's name
         * @param Phone - String : Patient's phone number
         * @param email - String : Patient's email
         * @param Address - String : Patient's Address
         * @param birthday - String : Patient's birthdate
         */
        this.Health_Number=hnum;
        super.setName(Name);
        super.setPhone(Phone);
        super.setEmail(email);
        this.Address = Address;
        this.Patient_name = Name;
        this.Birth_date = birthday;
    }

    public String getAddress() {
        return Address;
    }

    public String getPatient_name() {
        return Patient_name;
    }

    public Integer getHealth_number() {
        return Health_Number;
    }

    public String getGender() {
        return Gender;
    }

    public LocalDate getBirth_date() {
        return Birth_date;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public void setHealth_Number(Integer Health_NUmber) {
        this.Health_Number = Health_NUmber;
    }

    public void setBirth_date(LocalDate birth) {
        this.Birth_date = birth;
    }

    public void setAge(String Gender) {
        this.Gender = Gender;
    }

    public void setPatient_name(String name) {
        Patient_name = name;
    }
}