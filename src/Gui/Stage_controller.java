package Gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class Stage_controller {
    public Stage stage = new Stage();
    private Parent root;

    public void setStage(String file, String title, int dimensions_x,int dimensions_y, boolean clean_bar) throws IOException {
        root = FXMLLoader.load(getClass().getResource(file));
        this.stage.setTitle(title);
        this.stage.setScene(new Scene(root, dimensions_x, dimensions_y));

        if(clean_bar){
            this.stage.initStyle(StageStyle.UNDECORATED);
        }
    }
    public void showStage(){
        this.stage.show();
    }
    public void closeStage(){
         this.stage.close();
    }

    public void modality(Modality type){
        this.stage.initModality(type);
    }
}
