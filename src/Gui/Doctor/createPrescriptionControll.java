package Gui.Doctor;

import DB.Data_base;
import Login.Login;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class createPrescriptionControll implements Initializable {

    @FXML
    private Label Address_label;

    @FXML
    private ImageView CloseApp;

    @FXML
    private Label Date_label;

    @FXML
    private Label Doctor_label;

    @FXML
    private Label Patient_label;

    @FXML
    private Label active_subs_label;

    @FXML
    private AnchorPane bar;

    @FXML
    private GridPane borders;

    @FXML
    private TextArea commment_text;

    @FXML
    private Button create_button;

    @FXML
    private TextField dose_text;

    @FXML
    private TextField duration_text;

    @FXML
    private Label exp_date;

    @FXML
    private Label expiration_date_label;

    @FXML
    private ComboBox<String> medicine_choise;

    @FXML
    private Label medicine_label;

    @FXML
    private TextField quantity_text;


    private Consumer<String> callback;


    int health_number=0, drug=0,dr_id=0;



    ResultSet rs = null;
    String [][] medicine = null;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CloseApp.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();
        });

        getAllMeds();

        set_medicine_choise();

        medicine_choise.setOnAction((event) -> {
            int selectedIndex = medicine_choise.getSelectionModel().getSelectedIndex();
            Object selectedItem = medicine_choise.getSelectionModel().getSelectedItem();
            try {
                active_subs_label.setText(Data_base.getActive_subs(Integer.parseInt(medicine[selectedIndex][0])));
                drug = Integer.parseInt(medicine[selectedIndex][0]);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }

    public void getPatient(int health_number){
        this.health_number = health_number;

        init_labels();
    };

    public void getAllMeds(){
        try {
            medicine = Data_base.getMedicine();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void set_medicine_choise(){
        int i=0;
        for(i=0;i<medicine.length;i++){
            medicine_choise.getItems().add("#"+medicine[i][0]+" "+medicine[i][1]);
        }
    }


    public void init_labels() {
        try {
            rs = Data_base.getPatient(health_number);
            while (rs.next()) {
                Patient_label.setText("                     " +rs.getString("name"));
                Address_label.setText("                      " +rs.getString("address"));
                Date_label.setText(LocalDate.now().toString());
               rs = Data_base.getDoctor(Login.username);
                while (rs.next()) {
                    dr_id = rs.getInt("id");
                    Doctor_label.setText(rs.getString("name"));
                }
                exp_date.setText(LocalDate.now().plusMonths(1).toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setup(Consumer<String> callback) {
        this.callback = callback;
    }

    @FXML
    void create_button_action(ActionEvent event) throws SQLException {

        if (medicine_choise.getSelectionModel().getSelectedItem()!=null && !quantity_text.getText().isBlank() && !duration_text.getText().isBlank() &&
        !dose_text.getText().isBlank()){
            Data_base.CreatePrescription( dose_text.getText(),duration_text.getText(), quantity_text.getText(),
                    exp_date.getText(), commment_text.getText(),drug,dr_id,health_number,LocalDate.now().toString() );
            callback.accept("create");
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();

        }
    }

    @FXML
    void quantity_action(KeyEvent event) {
        if (!((int) event.getCharacter().charAt(0) >= 48 && (int) event.getCharacter().charAt(0) < 58) && (int) event.getCharacter().charAt(0) !=8) {
            quantity_text.deletePreviousChar();
        }
    }

}

