package Gui.admin;

import DB.Data_base;
import Gui.Stage_controller;
import Users.*;
import Actions.*;
import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;

import java.net.URL;
import java.util.ResourceBundle;

public class menu_admin_controller implements Initializable {
    @FXML
    private ImageView CloseApp;

    @FXML
    private Pane patient_pane, doctor_pane, pharmacy_pane, medicine_pane, charts_pane;

    @FXML
    private Button btn_p, btn_d, btn_ph, btn_m, btn_c, logout;

    @FXML
    private Label u_num, p_num, d_num, ph_num, m_num, app_num, presc_num;

    @FXML
    private TextField tf_p, tf_d, tf_ph, tf_m;

    @FXML
    private PieChart pie;

    @FXML
    private BarChart<String, Integer> bar;

    public int num_p, num_d, num_ph, num_m, num_u;

    /** PACIENTS TABLE **/
    @FXML
    private TableView <Patient> t_pacient;
    @FXML
    private TableColumn<Patient, String> p_name, p_phone, p_mail;
    @FXML
    private TableColumn<Patient, Integer> p_hnum;
    @FXML
    private TableColumn<Patient, Void> pat_button = new TableColumn<>("Actions");
    private ObservableList<Patient> data_p;

    /** DOCTORS TABLE **/
    @FXML
    private TableView <Doctor> t_doctor;
    @FXML
    private TableColumn<Doctor, String> d_name, d_phone, d_speciality, d_clinic;
    @FXML
    private TableColumn<Doctor, Integer> d_id;
    @FXML
    private TableColumn<Doctor, Void> doc_button = new TableColumn<>("Actions");
    public ObservableList<Doctor> data_d;

    /** PHARMACY TABLE **/
    @FXML
    private TableView <Pharmacy> t_pharma;
    @FXML
    private TableColumn<Pharmacy, String> ph_name, ph_phone, ph_address;
    @FXML
    private TableColumn<Pharmacy, Integer> ph_id;
    @FXML
    private TableColumn<Pharmacy, Void> ph_button = new TableColumn<>("Actions");
    public ObservableList<Pharmacy> data_ph;

    /** MEDICINES TABLE **/
    @FXML
    private TableView <Medicine> t_medicine;
    @FXML
    private TableColumn<Medicine, String> m_name, m_acts;
    @FXML
    private TableColumn<Medicine, Integer>  m_code;
    @FXML
    private TableColumn<Medicine, Void> med_button = new TableColumn<>("Actions");
    public ObservableList<Medicine> data_m;

    private Stage_controller new_stage = new Stage_controller();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setAllInvisible();
        charts_pane.setVisible(true);
        inicializetables();
        searchbars();
        gotocharts();
        CloseApp.setOnMouseClicked(event -> {
            System.exit(0);
        });
        logout.setOnMouseClicked(event -> {
            try{
                new_stage.setStage("login/log in.fxml", "Halland", 568, 400, false);
            } catch(IOException e){
                e.printStackTrace();
            }

            new_stage.showStage();
            Stage menu = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            menu.close();
        });

    }

    public void refresh(){
        t_pacient.getColumns().remove(4);
        t_doctor.getColumns().remove(5);
        t_pharma.getColumns().remove(4);
        t_medicine.getColumns().remove(3);
        inicializetables();
        searchbars();
    }

    public void setPieChart(){
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
                new PieChart.Data("Patients", num_p),
                new PieChart.Data("Doctors", num_d),
                new PieChart.Data("Pharmacies", num_ph)
        );
        num_u=num_p+num_d+num_ph;
        pie.getData().clear();
        pie.setData(pieChartData);
        pie.setLabelsVisible(false);
    }

    public void setBarChart(){
        XYChart.Series<String, Integer> series = new XYChart.Series<>();

        try{
            series.getData().add(new XYChart.Data<>("Appointments", Data_base.getApp_num()));
            series.getData().add(new XYChart.Data<>("Prescriptions", Data_base.getPresc_num()));
        } catch (SQLException e) {
            e.printStackTrace();
        }


        bar.getData().clear();
        bar.getData().add(series);
    }

    public void insertnewmed(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Gui/admin/popup_newmedicine.fxml"));
        Parent parent = loader.load();

        newmedicine_controller controller = (newmedicine_controller) loader.getController();

        controller.setup(
                (String value)->{if(value.equals("ref")){
                    refresh();
                }
                });

        Stage stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(parent,280,190));
        stage.show();
    }

    public void gotopacients (ActionEvent event) {
        setAllInvisible();
        setallbuttons();
        patient_pane.setVisible(true);
        changeStyleCLass(btn_p,"pressed");
    }

    public void gotodoctors (ActionEvent event) {
        setallbuttons();
        setAllInvisible();
        doctor_pane.setVisible(true);
        changeStyleCLass(btn_d,"pressed");
    }

    public void gotomedicine (ActionEvent event) {
        setAllInvisible();
        setallbuttons();
        medicine_pane.setVisible(true);
        changeStyleCLass(btn_m,"pressed");
    }

    public void gotopharmacies (ActionEvent event) {
        setAllInvisible();
        setallbuttons();
        pharmacy_pane.setVisible(true);;
        changeStyleCLass(btn_ph,"pressed");
    }

    public void gotocharts(){
        setAllInvisible();
        setallbuttons();
        setPieChart();
        setBarChart();
        setlabels();
        charts_pane.setVisible(true);
        changeStyleCLass(btn_c,"pressed");

    }

    public void setlabels(){
        u_num.setText("  "+String.valueOf(num_u));
        p_num.setText("  "+String.valueOf(num_p));
        d_num.setText("  "+String.valueOf(num_d));
        ph_num.setText("  "+String.valueOf(num_ph));
        m_num.setText("  "+String.valueOf(num_m));

        try{
            app_num.setText("  "+String.valueOf(Data_base.getApp_num()));
            presc_num.setText("  "+String.valueOf(Data_base.getPresc_num()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setAllInvisible(){
        patient_pane.setVisible(false);
        doctor_pane.setVisible(false);
        pharmacy_pane.setVisible(false);
        medicine_pane.setVisible(false);
        charts_pane.setVisible(false);
    }

    public void setallbuttons(){
        changeStyleCLass(btn_p,"menu");
        changeStyleCLass(btn_d,"menu");
        changeStyleCLass(btn_ph,"menu");
        changeStyleCLass(btn_m,"menu");
        changeStyleCLass(btn_c,"menu");
    }

    public void changeStyleCLass(Button btn, String n_class){
        btn.getStyleClass().clear();
        btn.getStyleClass().add(n_class);
    }

    public void inicializetables(){
        p_hnum.setCellValueFactory(new PropertyValueFactory<>("health_number"));
        p_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        p_phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
        p_mail.setCellValueFactory(new PropertyValueFactory<>("email"));

        d_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        d_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        d_phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
        d_speciality.setCellValueFactory(new PropertyValueFactory<>("speciality"));
        d_clinic.setCellValueFactory(new PropertyValueFactory<>("clinic"));

        ph_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        ph_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        ph_phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
        ph_address.setCellValueFactory(new PropertyValueFactory<>("address"));

        m_code.setCellValueFactory(new PropertyValueFactory<>("code"));
        m_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        m_acts.setCellValueFactory(new PropertyValueFactory<>("substance"));

        try {
            data_p = getPacients();
            data_d = getAllDocs();
            data_ph = getAllPharma();
            data_m = getMedicinesList();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        t_pacient.setItems(data_p);
        addButtontoPat();
        t_doctor.setItems(data_d);
        addButtontoDoc();
        t_pharma.setItems(data_ph);
        addButtontoPharma();
        t_medicine.setItems(data_m);
        addButtontoMed();

    }

    public void searchbars(){
        FilteredList<Patient> filt_datap = new FilteredList<>(data_p, b->true);
        tf_p.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datap.setPredicate(patient -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_p = newValue.toLowerCase();
                if(patient.getName().toLowerCase().indexOf(lcf_p) !=-1){
                    return true;
                } else if(String.valueOf(patient.getHealth_number()).indexOf(lcf_p) != -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Patient> sort_p = new SortedList<>(filt_datap);
        sort_p.comparatorProperty().bind(t_pacient.comparatorProperty());
        t_pacient.setItems(sort_p);

        FilteredList<Doctor> filt_datad = new FilteredList<>(data_d, b->true);
        tf_d.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datad.setPredicate(doctor -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_d = newValue.toLowerCase();
                if(doctor.getName().toLowerCase().indexOf(lcf_d) !=-1){
                    return true;
                } else if(String.valueOf(doctor.getId()).indexOf(lcf_d) != -1){
                    return true;
                } else if(doctor.getSpeciality().toLowerCase().indexOf(lcf_d) != -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Doctor> sort_d = new SortedList<>(filt_datad);
        sort_d.comparatorProperty().bind(t_doctor.comparatorProperty());
        t_doctor.setItems(sort_d);

        FilteredList<Pharmacy> filt_dataph = new FilteredList<>(data_ph, b->true);
        tf_ph.textProperty().addListener((observable, oldValue, newValue)->{
            filt_dataph.setPredicate(pharmacy -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_ph = newValue.toLowerCase();
                if(pharmacy.getName().toLowerCase().indexOf(lcf_ph) !=-1){
                    return true;
                } else if(String.valueOf(pharmacy.getId()).indexOf(lcf_ph) != -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Pharmacy> sort_ph = new SortedList<>(filt_dataph);
        sort_ph.comparatorProperty().bind(t_pharma.comparatorProperty());
        t_pharma.setItems(sort_ph);

        FilteredList<Medicine> filt_datam = new FilteredList<>(data_m, b->true);
        tf_m.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datam.setPredicate(medicine -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_m = newValue.toLowerCase();
                if(medicine.getName().toLowerCase().indexOf(lcf_m) !=-1){
                    return true;
                } else if(String.valueOf(medicine.getCode()).indexOf(lcf_m) != -1){
                    return true;
                } else if(medicine.getSubstance().toLowerCase().indexOf(lcf_m) != -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Medicine> sort_m = new SortedList<>(filt_datam);
        sort_m.comparatorProperty().bind(t_medicine.comparatorProperty());
        t_medicine.setItems(sort_m);
    }

    public void openwarning(int id, int type) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Gui/admin/warning.fxml"));
        Parent parent = loader.load();

        warning_controller controller = (warning_controller) loader.getController();

        controller.getData(id,type);

        controller.setup(
                (String value)->{if(value.equals("ref")){
                    refresh();
                }
                });

        Stage stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(parent,330,150));
        stage.show();
    }

    private void addButtontoPat() {
        Callback<TableColumn<Patient, Void>, TableCell<Patient, Void>> cellFactory = new Callback<TableColumn<Patient, Void>, TableCell<Patient, Void>>() {
            @Override
            public TableCell<Patient, Void> call(final TableColumn<Patient, Void> param)  {
                final TableCell<Patient, Void> cell = new TableCell<Patient, Void>() {
                    ImageView del = new ImageView(getClass().getResource("/images/delete.png").toExternalForm());
                    private final JFXButton btn0 = new JFXButton();
                    {
                        btn0.setOnAction((ActionEvent event) -> {
                            Patient data = getTableView().getItems().get(getIndex());
                            try{
                                openwarning(data.getHealth_number(),0);
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn0, "btn_table");
                            btn0.setPrefSize(25,30);
                            del.setFitWidth(25);
                            del.setFitHeight(25);
                            btn0.setGraphic(del);
                            HBox hbox = new HBox(20,btn0);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        pat_button.setCellFactory(cellFactory);
        pat_button.setPrefWidth(100);
        t_pacient.getColumns().add(pat_button);
    }

    private void addButtontoDoc() {
        Callback<TableColumn<Doctor, Void>, TableCell<Doctor, Void>> cellFactory = new Callback<TableColumn<Doctor, Void>, TableCell<Doctor, Void>>() {
            @Override
            public TableCell<Doctor, Void> call(final TableColumn<Doctor, Void> param)  {
                final TableCell<Doctor, Void> cell = new TableCell<Doctor, Void>() {
                    ImageView del = new ImageView(getClass().getResource("/images/delete.png").toExternalForm());
                    private final JFXButton btn1 = new JFXButton();
                    {
                        btn1.setOnAction((ActionEvent event) -> {
                            Doctor data = getTableView().getItems().get(getIndex());
                            try{
                                openwarning(data.getId(),1);
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn1, "btn_table");
                            btn1.setPrefSize(25,30);
                            del.setFitWidth(25);
                            del.setFitHeight(25);
                            btn1.setGraphic(del);
                            HBox hbox = new HBox(20,btn1);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        doc_button.setCellFactory(cellFactory);
        doc_button.setPrefWidth(100);
        t_doctor.getColumns().add(doc_button);
    }

    private void addButtontoPharma() {
        Callback<TableColumn<Pharmacy, Void>, TableCell<Pharmacy, Void>> cellFactory = new Callback<TableColumn<Pharmacy, Void>, TableCell<Pharmacy, Void>>() {
            @Override
            public TableCell<Pharmacy, Void> call(final TableColumn<Pharmacy, Void> param)  {
                final TableCell<Pharmacy, Void> cell = new TableCell<Pharmacy, Void>() {
                    ImageView del = new ImageView(getClass().getResource("/images/delete.png").toExternalForm());
                    private final JFXButton btn1 = new JFXButton();
                    {
                        btn1.setOnAction((ActionEvent event) -> {
                            Pharmacy data = getTableView().getItems().get(getIndex());
                            try{
                                openwarning(data.getId(),2);
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn1, "btn_table");
                            btn1.setPrefSize(25,30);
                            del.setFitWidth(25);
                            del.setFitHeight(25);
                            btn1.setGraphic(del);
                            HBox hbox = new HBox(20,btn1);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        ph_button.setCellFactory(cellFactory);
        ph_button.setPrefWidth(100);
        t_pharma.getColumns().add(ph_button);
    }

    private void addButtontoMed() {
        Callback<TableColumn<Medicine, Void>, TableCell<Medicine, Void>> cellFactory = new Callback<TableColumn<Medicine, Void>, TableCell<Medicine, Void>>() {
            @Override
            public TableCell<Medicine, Void> call(final TableColumn<Medicine, Void> param)  {
                final TableCell<Medicine, Void> cell = new TableCell<Medicine, Void>() {
                    ImageView del = new ImageView(getClass().getResource("/images/delete.png").toExternalForm());
                    private final JFXButton btn1 = new JFXButton();
                    {
                        btn1.setOnAction((ActionEvent event) -> {
                            Medicine data = getTableView().getItems().get(getIndex());
                            try{
                                openwarning(data.getCode(),3);
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn1, "btn_table");
                            btn1.setPrefSize(25,30);
                            del.setFitWidth(25);
                            del.setFitHeight(25);
                            btn1.setGraphic(del);
                            HBox hbox = new HBox(20,btn1);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        med_button.setCellFactory(cellFactory);
        med_button.setPrefWidth(100);
        t_medicine.getColumns().add(med_button);
    }

    public ObservableList<Patient> getPacients() throws SQLException, ParseException{
        ObservableList<Patient> data =FXCollections.observableArrayList();

        ResultSet rs = Data_base.getStuff("SELECT health_number,name,phone,email,address,birthday FROM halland.patient");

        num_p=0;
        LocalDate birth;
        String birth_s;
        Integer aux;

        while(rs.next()){
            num_p++;
            birth_s = rs.getString("birthday");
            birth = LocalDate.parse(birth_s);
            aux=Integer.parseInt(rs.getString("health_number"));
            data.add(new Patient(aux, rs.getString("name"), rs.getString("phone"),
                    rs.getString("email"),rs.getString("address"),birth));
        }

        return data;
    }

    public ObservableList<Doctor> getAllDocs() throws SQLException, ParseException{
        ObservableList<Doctor> data =FXCollections.observableArrayList();

        ResultSet rs = Data_base.getStuff("SELECT id,name,phone,email,speciality,clinic FROM halland.doctor");

        Integer aux;
        num_d=0;

        while(rs.next()){
            num_d++;
            aux=Integer.parseInt(rs.getString("id"));
            data.add(new Doctor(aux, rs.getString("name"), rs.getString("phone"),
                    rs.getString("email"),rs.getString("speciality"),
                    rs.getString("clinic")));
        }

        return data;
    }

    public ObservableList<Pharmacy> getAllPharma() throws SQLException, ParseException{
        ObservableList<Pharmacy> data =FXCollections.observableArrayList();

        ResultSet rs = Data_base.getStuff("SELECT id,name,phone,address,email FROM halland.pharmacy");

        Integer aux;
        num_ph=0;

        while(rs.next()){
            num_ph++;
            aux=Integer.parseInt(rs.getString("id"));
            data.add(new Pharmacy(aux, rs.getString("name"), rs.getString("phone"),
                    rs.getString("email"),rs.getString("address")));
        }

        return data;
    }

    public ObservableList<Medicine> getMedicinesList() throws SQLException, ParseException {
        ObservableList<Medicine> data =FXCollections.observableArrayList();

        ResultSet rs = Data_base.getStuff("SELECT * FROM halland.medicine");
        num_m=0;
        int aux;

        while(rs.next()){
            num_m++;
            aux=Integer.parseInt(rs.getString("code"));
            data.add(new Medicine(rs.getString("name"),aux, rs.getString("act_substance")));
        }

    return data;
    }

}
