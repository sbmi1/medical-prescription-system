package Gui.admin;

import DB.Data_base;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class newmedicine_controller implements Initializable {

    @FXML
    private ImageView CloseApp;

    @FXML
    private TextField tf_name, tf_acts;

    @FXML
    private Label warning;

    private Consumer<String> callback;

    public int text;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        warning.setVisible(false);
        CloseApp.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();
        });
    }

    public void hidelabel(){
        warning.setVisible(false);
        changeStyleCLass("warning");
    }

    public void request(ActionEvent event) throws SQLException{

        boolean aux;
        if(tf_name.getText().isBlank() || tf_acts.getText().isBlank()){
            warning.setText("Please complete the form");
            warning.setVisible(true);
            return;
        }

        aux=Data_base.searchmed(tf_name.getText());

        if(aux){
            warning.setText("Medicine already registered");
            warning.setVisible(true);
            return;
        }

        Data_base.inssertmed(tf_name.getText(), tf_acts.getText());
        changeStyleCLass("success");
        warning.setText("Medicine added successfully!");
        warning.setVisible(true);

        callback.accept("ref");

        tf_name.setText("");
        tf_acts.setText("");
    }

    public void changeStyleCLass(String n_class){
        warning.getStyleClass().clear();
        warning.getStyleClass().add(n_class);
    }

    public void setup(Consumer<String> callback) {
        this.callback = callback;
    }
}

