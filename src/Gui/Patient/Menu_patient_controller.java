package Gui.Patient;

import Actions.Appointment;
import Actions.Prescription;
import DB.Data_base;
import Login.Login;
import com.jfoenix.controls.JFXButton;
import javafx.animation.Animation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import Gui.Stage_controller;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import javax.swing.*;

public class Menu_patient_controller implements Initializable {

    @FXML
    private ImageView CloseApp;

    //tabela appointments
    @FXML
    private TableView<Appointment> TabelaAppointments;
    @FXML
    private TableColumn<Appointment, Date> Col_Data;
    @FXML
    private TableColumn<Appointment, String> Col_Medico;
    @FXML
    private TableColumn<Appointment, String> Col_especialidade;
    @FXML
    private TableColumn<Appointment, String> Col_Clinica;
    @FXML
    private TableColumn<Appointment, String> Col_Time;

    @FXML
    private Button NewAppointmentButton;
    //tabela appointments menu
    @FXML
    private TableView<Appointment> TabelaAppointments_menu;
    @FXML
    private TableColumn<Appointment, String> Col_Clinica_menu;
    @FXML
    private TableColumn<Appointment, String> Col_Time_menu;
    @FXML
    private TableColumn<Appointment, Date> Col_Data_menu;
    @FXML
    private TableColumn<Appointment, Void>  app_button = new TableColumn<>("Delete");

    //tabela prescriptions
    @FXML
    private TableView<Prescription> TabelaPrescriptions;

    //tabela prescription menu
    @FXML
    private TableView<Prescription> TabelaPrescriptions_menu;
    @FXML
    private TableColumn<Prescription, Integer> col_quantity_menu;
    @FXML
    private TableColumn<Prescription, LocalDate> col_exp_date_menu;
    @FXML
    private TableColumn<Prescription, String> col_name_menu;
    @FXML
    private TableColumn<Prescription, Void>  col_prescription_menu_goTo = new TableColumn<>("Actions");

    //tabela medicamentos menu
    @FXML
    private TableView<Prescription> table_drugs_menu;
    @FXML
    private TableColumn<Prescription, Void>  col_drug_menu_goTo = new TableColumn<>("Delete");
    @FXML
    private TableColumn<Prescription, String> col_drug_dose_menu;
    @FXML
    private TableColumn<Prescription, Integer> col_drug_duration_menu;
    @FXML
    private TableColumn<Prescription, String> col_drug_name_menu;

    //other things
    @FXML
    private TableColumn<Prescription, Integer> col_code;

    @FXML
    private TableColumn<Prescription, Integer> col_quantity;

    @FXML
    private TableColumn<Prescription, LocalDate> col_exp_date;

    @FXML
    private TableColumn<Prescription, String> col_name;
    @FXML
    private TableColumn<Prescription, Void> col_Botao_verPresc = new TableColumn<>("Actions");

    @FXML
    private TableView<Prescription> table_drugs;

    @FXML
    private TableColumn<Prescription, Void>  drug_action_col = new TableColumn<>("Delete");

    @FXML
    private TableColumn<Prescription, String> drug_comment_col;

    @FXML
    private TableColumn<Prescription, String> drug_dose_col;

    @FXML
    private TableColumn<Prescription, Integer> drug_duration_col;

    @FXML
    private TableColumn<Prescription, String> drug_name_col;


    /************  Profile variables  **************/
    @FXML
    private ImageView Pass_Wrong, Pass_Confirm, Name_Wrong, Name_Confirm, Number_Confirm, Number_Wrong, Email_Confirm, Email_Wrong, Address_Confirm, Address_Wrong;
    @FXML
    private PasswordField password, password_conf;
    @FXML
    private ImageView Name_Change, Number_Change, Email_Change, Address_Change, Pass_Change;
    @FXML
    private TextField address_text, email_text, number_text, name_text;
    @FXML
    private Label username_text, LabelCheckPass, LabelPassInfo, LabelNumberInfo;
    private String newName, newPass, newNumber, newEmail, newAddress;
    private Boolean phoneOK;

    /***********  end of Profile variables  ********/

    @FXML
    private TextField tf_app, tf_presc, tf_med;
    @FXML
    private Label Welcome_text;

    @FXML
    private Button logout;

    ObservableList<Appointment> listApp;
    ObservableList<Prescription> listPres;
    ObservableList<Prescription> listMed;
    ObservableList<Appointment> listApp_menu;
    ObservableList<Prescription> listPres_menu;
    ObservableList<Prescription> listMed_menu;

    ResultSet rs = null;

    int UserId;

    private Stage_controller new_stage = new Stage_controller();

    @FXML
    Button b_p,b_menu,b_a,b_pro,b_m;

    @FXML
    private Pane MainMenu_pane, Prescriptions_Pane, Appointments_pane, Profile_pane,Drugs_pane;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SetAllVisible(false);
        MainMenu_pane.setVisible(true);
        CloseApp.setOnMouseClicked(event -> {
            System.exit(0);
        });

        logout.setOnMouseClicked(event -> {
            try{
                new_stage.setStage("login/log in.fxml", "Halland", 568, 400, false);
            } catch(IOException e){
                e.printStackTrace();
            }

            new_stage.showStage();
            Stage menu = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            menu.close();
        });


        table();
        searchbar();
        setallbuttons();
        changeStyleCLass(b_menu, "pressed");
    }

    public void refresh(){
        tf_presc.setText("");
        tf_app.setText("");
        tf_med.setText("");
        TabelaAppointments.getColumns().remove(5);
        TabelaPrescriptions.getColumns().remove(4);
        table_drugs.getColumns().remove(4);
        table();
        searchbar();
    }

    public void table(){
        Col_Data.setCellValueFactory(new PropertyValueFactory<>("date"));
        Col_Medico.setCellValueFactory(new PropertyValueFactory<>("Doctor_name"));
        Col_Clinica.setCellValueFactory(new PropertyValueFactory<>("clinic"));
        Col_especialidade.setCellValueFactory(new PropertyValueFactory<>("speciality"));
        Col_Time.setCellValueFactory(new PropertyValueFactory<>("time"));

        col_code.setCellValueFactory(new PropertyValueFactory<>("Code"));
        col_name.setCellValueFactory(new PropertyValueFactory<>("Drug"));
        col_exp_date.setCellValueFactory(new PropertyValueFactory<>("Expiration_date"));
        col_quantity.setCellValueFactory(new PropertyValueFactory<>("Quantity"));

        drug_name_col.setCellValueFactory(new PropertyValueFactory<>("Drug"));
        drug_dose_col.setCellValueFactory(new PropertyValueFactory<>("Dose"));
        drug_duration_col.setCellValueFactory(new PropertyValueFactory<>("Duration"));
        drug_comment_col.setCellValueFactory(new PropertyValueFactory<>("Comment"));

        drug_dose_col.setCellFactory(tc -> {
            TableCell<Prescription, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(drug_dose_col.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            text.setTextAlignment(TextAlignment.CENTER);
            return cell ;
        });

        drug_comment_col.setCellFactory(tc -> {
            TableCell<Prescription, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(drug_comment_col.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            text.setTextAlignment(TextAlignment.CENTER);
            return cell ;
        });

        Col_Time_menu.setCellValueFactory(new PropertyValueFactory<>("time"));
        Col_Data_menu.setCellValueFactory(new PropertyValueFactory<>("date"));
        Col_Clinica_menu.setCellValueFactory(new PropertyValueFactory<>("clinic"));

        col_name_menu.setCellValueFactory(new PropertyValueFactory<>("Drug"));
        col_exp_date_menu.setCellValueFactory(new PropertyValueFactory<>("Expiration_date"));
        col_quantity_menu.setCellValueFactory(new PropertyValueFactory<>("Quantity"));

        col_drug_name_menu.setCellValueFactory(new PropertyValueFactory<>("Drug"));
        col_drug_dose_menu.setCellValueFactory(new PropertyValueFactory<>("Dose"));
        col_drug_duration_menu.setCellValueFactory(new PropertyValueFactory<>("Duration"));

        try {
            rs = Data_base.getPatient(Login.username);
            while (rs.next()){
                UserId = rs.getInt("health_number");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            listApp = getDataAppointment(UserId);
            listPres = getDataPrescription(UserId);
            listMed = getDataDrugs(UserId);
            listApp_menu = getDataNAppointment(UserId, 1);
            listPres_menu = getDataNPrescription(UserId, null, 2);
            listMed_menu = getDataNDrugs(UserId, null, 2);

            String name;
            rs = Data_base.getPatient(UserId);
            rs.next();
            name = rs.getString("name");
            String split [] = name.split(" ", 2);
            name = split[0];
            Welcome_text.setText("Welcome Back, " + name + "!");

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        TabelaAppointments.setItems(listApp);
        addbuttonToAppointment();
        app_button.setPrefWidth(100);
        TabelaPrescriptions.setItems(listPres);
        add2ButtonToTable();
        col_Botao_verPresc.setPrefWidth(216);
        table_drugs.setItems(listMed);
        addButtonToTable();
        drug_action_col.setPrefWidth(99);
        TabelaAppointments_menu.setItems(listApp_menu);
        TabelaPrescriptions_menu.setItems(listPres_menu);
        table_drugs_menu.setItems(listMed_menu);

        try {
            fillProfileFieldsFromDB();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void searchbar(){
        FilteredList<Appointment> filt_datapp = new FilteredList<>(listApp, b->true);
        tf_app.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datapp.setPredicate(appointment -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_app = newValue.toLowerCase();
                if(appointment.getSpeciality().toLowerCase().indexOf(lcf_app) !=-1){
                    return true;
                } else if(String.valueOf(appointment.getDate()).toLowerCase().indexOf(lcf_app)!= -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Appointment> sort_aap = new SortedList<>(filt_datapp);
        sort_aap.comparatorProperty().bind(TabelaAppointments.comparatorProperty());
        TabelaAppointments.setItems(sort_aap);

        FilteredList<Prescription> filt_datap = new FilteredList<>(listPres, b->true);
        tf_presc.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datap.setPredicate(prescription -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_presc = newValue.toLowerCase();
                if((prescription.getDrug().toLowerCase().indexOf(lcf_presc)) !=-1){
                    return true;
                } else if(String.valueOf(prescription.getExpiration_date()).toLowerCase().indexOf(lcf_presc)!= -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Prescription> sort_presc = new SortedList<>(filt_datap);
        sort_presc.comparatorProperty().bind(TabelaPrescriptions.comparatorProperty());
        TabelaPrescriptions.setItems(sort_presc);

        FilteredList<Prescription> filt_datam = new FilteredList<>(listMed, b->true);
        tf_med.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datam.setPredicate(prescription -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_med = newValue.toLowerCase();
                if((prescription.getDrug().toLowerCase().indexOf(lcf_med)) !=-1){
                    return true;
                } else if(String.valueOf(prescription.getExpiration_date()).toLowerCase().indexOf(lcf_med)!= -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Prescription> sort_med = new SortedList<>(filt_datam);
        sort_med.comparatorProperty().bind(table_drugs.comparatorProperty());
        table_drugs.setItems(sort_med);

    }

    public void goToMenuOnAction() {
        SetAllVisible(false);
        MainMenu_pane.setVisible(true);
        setallbuttons();
        changeStyleCLass(b_menu, "pressed");
    }

    public void goToPrescriptionOnAction() {
        SetAllVisible(false);
        Prescriptions_Pane.setVisible(true);
        setallbuttons();
        changeStyleCLass(b_p, "pressed");
    }

    public void goToAppointmentOnAction()  {
        SetAllVisible(false);
        Appointments_pane.setVisible(true);
        setallbuttons();
        changeStyleCLass(b_a, "pressed");
    }

    public void goToDrugsOnAction() throws SQLException, ParseException {
        SetAllVisible(false);
        Drugs_pane.setVisible(true);
        listMed = getDataDrugs(UserId);
        setallbuttons();
        changeStyleCLass(b_m, "pressed");
    }

    public void goToProfileOnAction() {
        SetAllVisible(false);
        Profile_pane.setVisible(true);
        setIconsVisible(false);
        setFieldsDisable(true);
        password.setDisable(true);
        password_conf.setDisable(true);
        setallbuttons();
        changeStyleCLass(b_pro, "pressed");
    }

    public void SetAllVisible(boolean b) {
        MainMenu_pane.setVisible(b);
        Prescriptions_Pane.setVisible(b);
        Appointments_pane.setVisible(b);
        Profile_pane.setVisible(b);
        Drugs_pane.setVisible(b);
    }

    public void NewAppointmentOnClick() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Gui/Patient/popup_newappoint.fxml"));
        Parent parent = loader.load();

        NewAppoint_controller controller =  loader.getController();

        controller.setup(
                (String value)->{if(value.equals("ref")){
                    refresh();
                }
                });

        Stage stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(parent,300,520));
        stage.show();
    }

    public void deletewarning(Prescription presc, Appointment app, int type, Stage print) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Gui/Patient/popup_warning.fxml"));
        Parent parent = loader.load();

        popup_warning_controller controller = loader.getController();

        if(presc==null){
            controller.getData(UserId, -1, Integer.parseInt(app.getCode()), type, null);
        }
        else{
            controller.getData(UserId, presc.getCode(), -1,type,print);
        }

        controller.setup(
                (String value)->{if(value.equals("delete")){
                    refresh();
                }
                });

        Stage stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(parent,330,150));
        stage.show();
    }

    public static ObservableList<Appointment> getDataAppointment(int user) throws SQLException, ParseException {
        ObservableList<Appointment> list = FXCollections.observableArrayList();
        LocalDate Data_rs;
        String Data_s;
        ResultSet doc, pat;

        ResultSet rs = Data_base.getAppointments(user);

        while (rs.next()) {
            Data_s = rs.getString("Date");
            Data_rs = LocalDate.parse(Data_s);
            doc = Data_base.getDoctor(Integer.parseInt(rs.getString("doctor")));
            doc.next();
            pat = Data_base.getPatient(Integer.parseInt(rs.getString("patient")));
            pat.next();

            list.add(new Appointment(rs.getString("code"), rs.getString("clinic"), rs.getString("speciality"),Data_rs, Integer.parseInt(rs.getString("patient")), doc.getString("name"), pat.getString("name"), rs.getString("time")));
        }

        return list;
    }

    public static ObservableList<Prescription> getDataPrescription(int user) throws SQLException, ParseException {
        ObservableList<Prescription> list = FXCollections.observableArrayList();
        LocalDate Data_rs;
        String Data_s;
        ResultSet rs;

        rs = Data_base.getPrescriptions(user, "active");

        ResultSet rs1;
        String meds=null;
        while (rs.next()) {
            Data_s = rs.getString("expirationdate");
            Data_rs = LocalDate.parse(Data_s);
            rs1 = Data_base.getMedicine(rs.getInt("medicine"));
            while (rs1.next()){
                meds = rs1.getString("name");
            }
            list.add(new Prescription(rs.getInt("code"), null, null, rs.getInt("quantity"), Data_rs, null, meds,null,null,null,null));
        }

        return list;
    }

    public static ObservableList<Prescription> getDataDrugs(int user) throws SQLException, ParseException {
        ObservableList<Prescription> list = FXCollections.observableArrayList();
        ResultSet rs;

        rs = Data_base.getPrescriptions(user,"in use");

        ResultSet rs1 ;
        String meds = null;

        while (rs.next()) {
            rs1 = Data_base.getMedicine(rs.getInt("medicine"));
            while (rs1.next()){
                meds = rs1.getString("name");
            }
            list.add(new Prescription(rs.getInt("code"), rs.getString("dose"), rs.getString("duration"), 0, null, rs.getString("comment"), meds,null,null,null,null));
        }

        return list;
    }

    //lists for menu
    public static ObservableList<Prescription> getDataNDrugs(int user, String drug_name, int N) throws SQLException, ParseException {
        ObservableList<Prescription> list = FXCollections.observableArrayList();
        ResultSet rs;
        if(drug_name==null){
            rs = Data_base.getPrescriptions(user,"in use");}
        else{
            rs = Data_base.getPrescriptions(user,"in use",drug_name);}

        ResultSet rs1 ;
        String meds = null;
        int cont = 0;

        while (rs.next() && cont < N) {
            rs1 = Data_base.getMedicine(rs.getInt("medicine"));
            while (rs1.next()){
                meds = rs1.getString("name");
            }
            list.add(new Prescription(rs.getInt("code"), rs.getString("dose"), rs.getString("duration"), 0, null, rs.getString("comment"), meds,null,null,null,null));
            cont++;
        }

        return list;
    }

    public static ObservableList<Prescription> getDataNPrescription(int user,String drug_name, int N) throws SQLException, ParseException {
        ObservableList<Prescription> list = FXCollections.observableArrayList();
        LocalDate Data_rs;
        String Data_s;
        ResultSet rs;
        if(drug_name==null){
            rs = Data_base.getPrescriptions(user, "active");}
        else {
            rs = Data_base.getPrescriptions(user, "active",drug_name);
        }
        int cont = 0;
        ResultSet rs1;
        String meds=null;
        while (rs.next() && cont < N) {
            Data_s = rs.getString("expirationdate");
            Data_rs = LocalDate.parse(Data_s);
            rs1 = Data_base.getMedicine(rs.getInt("medicine"));
            rs1.next();
            meds = rs1.getString("name");
            list.add(new Prescription(rs.getInt("code"), null, null, rs.getInt("quantity"), Data_rs, null, meds,null,null,null,null));
            cont++;
        }

        return list;
    }

    public static ObservableList<Appointment> getDataNAppointment(int user, int N) throws SQLException, ParseException {
        ObservableList<Appointment> list = FXCollections.observableArrayList();
        LocalDate Data_rs;
        String Data_s;
        ResultSet doc, pat;

        ResultSet rs = Data_base.getAppointments(user);
        int cont = 0;

        while (rs.next() && cont < N) {
            Data_s = rs.getString("Date");
            Data_rs = LocalDate.parse(Data_s);
            doc = Data_base.getDoctor(Integer.parseInt(rs.getString("doctor")));
            doc.next();
            pat = Data_base.getPatient(Integer.parseInt(rs.getString("patient")));
            pat.next();

            list.add(new Appointment(rs.getString("code"), rs.getString("clinic"), rs.getString("speciality"),Data_rs, Integer.parseInt(rs.getString("patient")), doc.getString("name"), pat.getString("name"), rs.getString("time")));
            cont++;
        }

        return list;
    }

    private void add2ButtonToTable() {

        Callback<TableColumn<Prescription, Void>, TableCell<Prescription, Void>> cellFactory = new Callback<TableColumn<Prescription, Void>, TableCell<Prescription, Void>>() {
            @Override
            public TableCell<Prescription, Void> call(final TableColumn<Prescription, Void> param) {
                final TableCell<Prescription, Void> cell = new TableCell<Prescription, Void>() {

                    ImageView delete = new ImageView(getClass().getResource("/images/delete.png").toExternalForm());
                    private final JFXButton btn = new JFXButton("Open");

                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Prescription data = getTableView().getItems().get(getIndex());
                            try {
                                openPrescription(data.getCode(),data);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    private final JFXButton btn2 = new JFXButton();

                    {
                        btn2.setOnAction((ActionEvent event) -> {
                            Prescription data = getTableView().getItems().get(getIndex());
                            try{
                                deletewarning(data, null,1,null);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn, "btn_table");
                            changeStyleCLass(btn2, "btn_table");
                            btn.setPrefSize(50,30);
                            btn2.setPrefSize(25,30);
                            delete.setFitWidth(25);
                            delete.setFitHeight(25);
                            btn2.setGraphic(delete);
                            HBox hbox = new HBox(20,btn,btn2);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        col_Botao_verPresc.setCellFactory(cellFactory);

        TabelaPrescriptions.getColumns().add(col_Botao_verPresc);

    }

    private void addButtonToTable()  {
        Callback<TableColumn<Prescription, Void>, TableCell<Prescription, Void>> cellFactory = new Callback<TableColumn<Prescription, Void>, TableCell<Prescription, Void>>() {
            @Override
            public TableCell<Prescription, Void> call(final TableColumn<Prescription, Void> param)  {
                final TableCell<Prescription, Void> cell = new TableCell<Prescription, Void>() {
                    ImageView del = new ImageView(getClass().getResource("/images/delete.png").toExternalForm());
                    private final JFXButton btn3 = new JFXButton();
                    {
                        btn3.setOnAction((ActionEvent event) -> {
                            Prescription data = getTableView().getItems().get(getIndex());
                            try{
                                deletewarning(data, null,0, null);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn3, "btn_table");
                            btn3.setPrefSize(25,30);
                            del.setFitWidth(25);
                            del.setFitHeight(25);
                            btn3.setGraphic(del);
                            HBox hbox = new HBox(20,btn3);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        drug_action_col.setCellFactory(cellFactory);
        table_drugs.getColumns().add(drug_action_col);

    }

    private void openPrescription(int code, Prescription presc) throws SQLException {
        FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource("/Gui/Patient/SeePrescription_Patient.fxml"));
        try {
            Parent root = fXMLLoader.load();
            SeePrescription_Patient_controller controller = fXMLLoader.getController();

            Stage stage = new Stage();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(new Scene(root,600,595));
            stage.show();

            controller.setup(
                    (String value)->{if(value.equals("delete")){
                        try{
                            deletewarning(presc, null, 1, stage);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    });

            controller.loadPrescription(code);


        } catch (IOException | SQLException ex) {
            Logger.getLogger(SeePrescription_Patient_controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setallbuttons(){
        changeStyleCLass(b_menu,"menu");
        changeStyleCLass(b_p,"menu");
        changeStyleCLass(b_a,"menu");
        changeStyleCLass(b_m,"menu");
        changeStyleCLass(b_pro,"menu");
    }

    public void changeStyleCLass(Button btn, String n_class){
        btn.getStyleClass().clear();
        btn.getStyleClass().add(n_class);
    }

    public void addbuttonToAppointment(){
        Callback<TableColumn<Appointment, Void>, TableCell<Appointment, Void>> cellFactory = new Callback<TableColumn<Appointment, Void>, TableCell<Appointment, Void>>() {
            @Override
            public TableCell<Appointment, Void> call(final TableColumn<Appointment, Void> param)  {
                final TableCell<Appointment, Void> cell = new TableCell<Appointment, Void>() {
                    ImageView del = new ImageView(getClass().getResource("/images/delete.png").toExternalForm());
                    private final JFXButton btn4 = new JFXButton();
                    {
                        btn4.setOnAction((ActionEvent event) -> {
                            Appointment data = getTableView().getItems().get(getIndex());
                            try{
                                deletewarning(null, data, 2, null);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn4, "btn_table");
                            btn4.setPrefSize(25,30);
                            del.setFitWidth(25);
                            del.setFitHeight(25);
                            btn4.setGraphic(del);
                            HBox hbox = new HBox(20,btn4);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        app_button.setCellFactory(cellFactory);
        TabelaAppointments.getColumns().add(app_button);
    }

    /*********************************** Profile Stuff ************************************/

    public void ProfileChangeName() throws SQLException {
        CancelChanges();
        name_text.setDisable(false);
        Name_Change.setVisible(false);
        Name_Confirm.setVisible(true);
        Name_Wrong.setVisible(true);
    }

    public void ProfileChangePassword() throws SQLException {
        CancelChanges();
        password.setDisable(false);
        password_conf.setDisable(false);
        Pass_Change.setVisible(false);
        Pass_Wrong.setVisible(true);
        Pass_Confirm.setVisible(true);
        LabelPassInfo.setText("Password between 6 and 15 digits");
        LabelPassInfo.setTextFill(Paint.valueOf("#000000"));
        LabelPassInfo.setVisible(true);
    }

    public void ProfileChangeNumber() throws SQLException {
        CancelChanges();
        number_text.setDisable(false);
        Number_Change.setVisible(false);
        Number_Confirm.setVisible(true);
        Number_Wrong.setVisible(true);
    }

    public void ProfileChangeEmail() throws SQLException {
        CancelChanges();
        email_text.setDisable(false);
        Email_Change.setVisible(false);
        Email_Confirm.setVisible(true);
        Email_Wrong.setVisible(true);
    }

    public void ProfileChangeAddress() throws SQLException {
        CancelChanges();
        address_text.setDisable(false);
        Address_Change.setVisible(false);
        Address_Confirm.setVisible(true);
        Address_Wrong.setVisible(true);
    }

    public void CancelChanges() throws SQLException {
        //dont forget to discard changes (dont update atual variables)
        fillProfileFieldsFromDB();
        setIconsVisible(false);
        setFieldsDisable(true);
    }

    public void setIconsVisible(boolean b){
        LabelNumberInfo.setVisible(b);
        LabelCheckPass.setVisible(b);
        LabelPassInfo.setVisible(b);
        Pass_Wrong.setVisible(b);
        Pass_Confirm.setVisible(b);
        Name_Wrong.setVisible(b);
        Name_Confirm.setVisible(b);
        Number_Wrong.setVisible(b);
        Number_Confirm.setVisible(b);
        Email_Wrong.setVisible(b);
        Email_Confirm.setVisible(b);
        Address_Wrong.setVisible(b);
        Address_Confirm.setVisible(b);
        Name_Change.setVisible(!b);
        Email_Change.setVisible(!b);
        Number_Change.setVisible(!b);
        Address_Change.setVisible(!b);
        Pass_Change.setVisible(!b);
    }

    public void setFieldsDisable(boolean b){
        password.setDisable(b);
        password_conf.setDisable(b);
        address_text.setDisable(b);
        email_text.setDisable(b);
        number_text.setDisable(b);
        name_text.setDisable(b);
    }

    public void fillProfileFieldsFromDB() throws SQLException {
        String sql;

        sql = "select name, address, email, phone, password from halland.patient where username = '" + Login.username + "'";
        rs = Data_base.getStuff(sql);
        rs.next();

        name_text.setText(rs.getString("name"));
        newName = name_text.getText();
        password.setText(rs.getString("password"));
        newPass = password.getText();
        password_conf.setText(password.getText());
        number_text.setText(rs.getString("phone"));
        newNumber = number_text.getText();
        email_text.setText(rs.getString("email"));
        newEmail = email_text.getText();
        address_text.setText(rs.getString("address"));
        newAddress = address_text.getText();
        username_text.setText(Login.username);
    }

    public void confirmChanges() throws SQLException {
        String name, pass, passConfirmation, phone, email, address;
        String first_name;
        Boolean sucess = false;

        name = name_text.getText();
        pass = password.getText();
        passConfirmation = password_conf.getText();
        phone = number_text.getText();
        email = email_text.getText();
        address = address_text.getText();

        if(!name_text.isDisable() && name.length() > 2){
            newName = name;
            String split [] = name.split(" ", 2);
            first_name = split[0];
            Welcome_text.setText("Welcome Back, " + first_name + "!");
            sucess = true;
        }
        else if(!password.isDisable()){
            if(checkPass(pass, passConfirmation)){
                newPass = pass;
                sucess = true;
            }
        }
        else if(!number_text.isDisable() && phoneOK){
                newNumber = phone;
                sucess = true;
        }
        else if(!email_text.isDisable()){
            newEmail = email;
            sucess = true;
        }
        else if(!address_text.isDisable()){
            newAddress = address;
            sucess = true;
        }
        if(sucess){
            setFieldsDisable(true);
            setIconsVisible(false);
            UpdateProfileOnDB();
        }

    }

    private boolean checkPass(String Pass_field, String PassConf_field){
        boolean retVal = true;

        if(Pass_field.length() < 6 || Pass_field.length() > 15){
            LabelPassInfo.setVisible(true);
            LabelPassInfo.setTextFill(Paint.valueOf("#eb0404"));
            LabelCheckPass.setVisible(false);
            retVal = false;
        }
        if(!Pass_field.equals(PassConf_field)){
            if(retVal) {
                LabelPassInfo.setVisible(false);
            }
            LabelCheckPass.setVisible(true);
            LabelCheckPass.setText("Passwords don't match!");
            LabelCheckPass.setTextFill(Paint.valueOf("#eb0404"));
            retVal = false;
        }
        return retVal;
    }

    public void phone_check(KeyEvent event){
        if (!((int) event.getCharacter().charAt(0) >= 48 && (int) event.getCharacter().charAt(0) < 58)) {
            number_text.deletePreviousChar();
        }
        if(number_text.getText().length()>=9){
            phoneOK = true;
            LabelNumberInfo.setVisible(false);
        }
        else{
            phoneOK = false;
            LabelNumberInfo.setVisible(true);
            LabelNumberInfo.setText("Must contain at least 9 numbers");
            LabelNumberInfo.setTextFill(Paint.valueOf("#eb0404"));
        }
    }

    public void UpdateProfileOnDB() throws SQLException {
        String sql;

        //update the fields of patient
        sql = "UPDATE halland.patient SET name='" + newName + "',password='" + newPass + "',phone='" + newNumber + "',email='" + newEmail +
                "',address='" + newAddress + "' where username='" + Login.username + "'";
        Data_base.updateDBfields(sql);

        sql = "UPDATE halland.users SET name='" + newName + "',password='" + newPass + "',phone='" + newNumber + "',email='" + newEmail + "' where username='" + Login.username + "'";
        Data_base.updateDBfields(sql);
    }

}

