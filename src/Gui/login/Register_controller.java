package Gui.login;

import DB.Data_base;
import Gui.Stage_controller;
import Login.Login;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;

public class Register_controller  extends Stage_controller {

    Login login = new Login();

    Boolean password_good, double_password_good, username_good, account_type, next_window, next_window_2, next_window_3, phone_chek, email_bool, health_number_bool, id_bool;
    @FXML
    private ChoiceBox gender;

    @FXML
    private CheckBox Doctor, Patient, Pharmacy;

    @FXML
    private Label label1, label2, label3, label4, label5, label6, label7, label_pass, label_user, label_pass2, label_registo, label_phone;

    @FXML
    private TextField text1, text2, text5, text6, text3;

    @FXML
    private PasswordField password1, password2;

    @FXML
    private DatePicker data;

    @FXML
    private Button next, back;

    @FXML
    private ImageView close;

    @FXML
    public void initialize() {
        text1.setVisible(false);
        text2.setVisible(false);
        text5.setVisible(false);
        text6.setVisible(false);
        password1.setVisible(false);
        password2.setVisible(false);
        label_pass.setVisible(false);
        label_user.setVisible(false);
        data.setVisible(false);
        next.setVisible(false);
        gender.setVisible(false);
        password_good = false;
        double_password_good = false;
        username_good = false;
        account_type = false;
        text3.setVisible(false);
        next_window = false;
        next_window_2 = false;
        next_window_3 = false;
        label_registo.setVisible(false);
        email_bool = false;
        label_phone.setVisible(false);
        health_number_bool = false;
        id_bool = false;

        close.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node) (event.getSource())).getScene().getWindow();
            Close.close();
        });
    }

    public void check_checkbox(ActionEvent event) {

        if (Doctor.isSelected() == true && event.getSource().equals(Doctor)) {
            Patient.setSelected(false);
            Pharmacy.setSelected(false);
        } else if (Patient.isSelected() == true && event.getSource().equals(Patient)) {
            Doctor.setSelected(false);
            Pharmacy.setSelected(false);
        } else if (Pharmacy.isSelected() == true && event.getSource().equals(Pharmacy)) {
            Patient.setSelected(false);
            Doctor.setSelected(false);
        } else {

            clear_common_register();
            account_type = false;
            return;
        }
        show_common_register();
        account_type = true;

    }

    public void show_common_register() {
        label1.setText("Name");
        text1.setVisible(true);
        label2.setText("Username");
        text2.setVisible(true);
        label3.setText("Password");
        password1.setVisible(true);
        label4.setText("Repeat Password");
        password2.setVisible(true);
        label5.setText("Phone Number");
        text5.setVisible(true);
        label6.setText("Email");
        text6.setVisible(true);
        label7.setText(" ");
        data.setVisible(false);
        label_phone.setVisible(true);
        label_pass.setText(" ");
        label_user.setText(" ");

        label_pass.setVisible(true);
        label_user.setVisible(true);
        gender.setVisible(false);
        next.setVisible(true);
        text3.setVisible(false);
    }

    public void clear_common_register() {
        label1.setText(" ");
        text1.setVisible(false);
        text1.clear();
        label2.setText(" ");
        text2.setVisible(false);
        text2.clear();
        label3.setText(" ");
        password1.setVisible(false);
        password1.clear();
        label4.setText(" ");
        password2.setVisible(false);
        password2.clear();
        label5.setText(" ");
        text5.setVisible(false);
        text5.clear();
        label6.setText(" ");
        text6.setVisible(false);
        text6.clear();
        label7.setText(" ");
        data.setVisible(false);
        label_pass.setVisible(false);
        label_user.setVisible(false);
        label_pass2.setVisible(false);
        next.setVisible(false);
        gender.setVisible(false);
        label_phone.setVisible(false);

    }

    public void next_window(ActionEvent event) throws SQLException {
        if (!text1.getText().isBlank() && !text2.getText().isBlank() && email_bool && phone_chek && double_password_good && username_good && account_type && !next_window && !next_window_2 && !next_window_3) {

            next_window = true;
            // continue ** FALTTA MUUDAR OS ATRIBUTOS E METODOS DE ACORDO COM O NOVO DIAGRAMA UML

            gender.getItems().clear();
            data.getEditor().clear();
            Doctor.setDisable(true);
            Pharmacy.setDisable(true);
            Patient.setDisable(true);
            label_phone.setVisible(false);


            if (Doctor.isSelected()) {
                login.user.setUser(text1.getText(), text5.getText(), text6.getText());
                login.setPassword(password1.getText());
                login.setUsername(text2.getText());

                label1.setText("Clinic");
                label2.setText("ID");
                label3.setText("");
                label4.setText(" ");
                label5.setText("Speciality");
                label6.setText("Gender");
                label7.setText("Birth Date");
                password1.setVisible(false);
                password2.setVisible(false);
                //text5.setVisible(false);
                data.setVisible(true);
                gender.setVisible(true);
                gender.getItems().add("Male");
                gender.getItems().add("Female");

                text3.setVisible(false);


                text1.clear();
                text2.clear();
                text5.clear();
                text3.clear();
            } else if (Patient.isSelected()) {
                login.user.setUser(text1.getText(), text5.getText(), text6.getText());
                login.setPassword(password1.getText());
                login.setUsername(text2.getText());

                label1.setText("");
                label2.setText("Address");
                label3.setText("Health Number");
                label4.setText("");
                label5.setText("");
                label6.setText("Gender");
                label7.setText("Birth Date");
                password1.setVisible(false);
                password2.setVisible(false);
                text1.setVisible(false);
                text5.setVisible(false);
                text2.setVisible(true);
                text3.setVisible(true);

                data.setVisible(true);
                gender.setVisible(true);
                gender.getItems().add("Male");
                gender.getItems().add("Female");
                text3.clear();
                text2.clear();

            } else if (Pharmacy.isSelected()) {
                login.user.setUser(text1.getText(), text5.getText(), text6.getText());
                login.setPassword(password1.getText());
                login.setUsername(text2.getText());

                label1.setText("");
                label2.setText("ID");
                label3.setText("Address");
                label4.setText("");
                label5.setText("");
                label6.setText("");
                label7.setText("");
                password1.setVisible(false);
                password2.setVisible(false);
                text1.setVisible(false);
                text2.setVisible(true);
                text3.setVisible(true);
                text5.setVisible(false);
                text6.setVisible(false);
                gender.setVisible(false);
                data.setVisible(false);

                text3.clear();
                text2.clear();

            }
        } else if (next_window) {

            if (Doctor.isSelected() && !text1.getText().isBlank() && id_bool
                    && !text5.getText().isBlank() && !gender.getSelectionModel().isEmpty() && !data.getEditor().getText().isBlank()) {

                login.novo_registo(login, text1.getText(), Integer.parseInt(text2.getText()), text5.getText()
                        , gender.getValue().toString(), data.getValue(), null, null, "Doctor");

            } else if (Patient.isSelected() && !text2.getText().isBlank() && health_number_bool
                    && !gender.getSelectionModel().isEmpty() && !data.getEditor().getText().isBlank()) {

                login.novo_registo(login, null, 0, null
                        , gender.getValue().toString(), data.getValue(), text2.getText(), text3.getText(), "Patient");

            } else if (Pharmacy.isSelected() && id_bool && !text3.getText().isBlank()) {
                login.novo_registo(login, null, Integer.parseInt(text2.getText()), null
                        , null, null, text3.getText(), null, "Pharmacy");
            } else {
                return;
            }

            next_window_2 = true;
            next_window = false;
            text1.setDisable(true);
            text2.setDisable(true);
            text3.setDisable(true);
            text5.setDisable(true);
            text6.setVisible(false);
            data.setDisable(true);
            gender.setDisable(true);

            next.setVisible(true);
            next.setText("Login");
            label_registo.setVisible(true);
            back.setVisible(false);

        } else if (next_window_2) {


            Stage_controller newstage = new Stage_controller();
            try {
                newstage.setStage("login/log in.fxml", "Halland", 568, 400, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
            newstage.showStage();
            Stage register = (Stage) ((Node) (event.getSource())).getScene().getWindow();
            register.close();

        }
    }

    public void check_user(KeyEvent event) throws SQLException {
        if (next_window == false) {
            if ((int) event.getCharacter().charAt(0) == 32)
                text2.deletePreviousChar();

            if (text2.getText().length() < 6 || text2.getText().length() > 15) {
                label_user.setVisible(true);
                label_user.setText("Username between 6 and 15 digits");
                label_user.setTextFill(Paint.valueOf("#eb0404"));
            } else {
                label_user.setTextFill(Paint.valueOf("#000000"));
                label_user.setText(" ");
                label_user.setVisible(false);
                if (login.validate_username_potentiality(text2.getText())) {
                    label_user.setVisible(false);
                    username_good = true;
                    return;
                } else {
                    label_user.setVisible(true);
                    label_user.setTextFill(Paint.valueOf("#eb0404"));
                    label_user.setText("Username is not available");
                }
            }

            if (text2.getText().length() == 0)
                label_user.setTextFill(Paint.valueOf("#000000"));
            username_good = false;
        } else if(Patient.isSelected() == false){
            if (!((int) event.getCharacter().charAt(0) >= 48 && (int) event.getCharacter().charAt(0) < 58)  && (int) event.getCharacter().charAt(0) != 8) {
                text2.deletePreviousChar();
                label_user.setVisible(false);

            }
            String auxiliar;
            if (Doctor.isSelected()) auxiliar = "Doctor";
            else auxiliar = "Pharmacy";
            if(event.getCharacter().charAt(0) == 8) return;
            if (Data_base.procura_id_unico(Integer.parseInt(text2.getText().toString()), auxiliar) ) {
                label_user.setText("Already in the system");
                label_user.setTextFill(Paint.valueOf("#eb0404"));
                label_user.setVisible(true);
                id_bool = false;

            } else {
                label_user.setTextFill(Paint.valueOf("#000000"));
                label_user.setVisible(false);
                if (!text2.getText().isBlank()) {
                    id_bool = true;
                }
            }
        }

    }

    public void check_pass(KeyEvent event) {
        if (password1.getText().length() < 6 || password1.getText().length() > 15) {
            label_pass.setVisible(true);
            label_pass.setText("Password between 6 and 15 digits");
            label_pass.setTextFill(Paint.valueOf("#eb0404"));
        } else {
            label_user.setTextFill(Paint.valueOf("#000000"));
            label_pass.setText(" ");
            label_pass.setVisible(false);
            password_good = true;
            if (!password2.getText().isBlank())
                check_confirm_password(event);
            return;
        }

        if (password1.getText().length() == 0)
            label_pass.setTextFill(Paint.valueOf("#000000"));

        password_good = false;
    }

    public void check_confirm_password(KeyEvent event) {
        if (password2.getText().equals(password1.getText()) && password_good) {
            label_pass2.setVisible(false);
            double_password_good = true;
            return;
        } else {
            label_pass2.setVisible(true);
            label_pass2.setText("Passwords don't match");
            label_pass2.setTextFill(Paint.valueOf("#eb0404"));
        }
        double_password_good = false;

    }

    public void back_stage(ActionEvent event) {
        if (!Doctor.isDisable()) {
            Stage stage = (Stage) back.getScene().getWindow();
            stage.close();

            Stage_controller Stage = new Stage_controller();
            try {
                Stage.setStage("login/log in.fxml", "Halland", 568, 400, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage.showStage();
            Stage menu = (Stage) ((Node) (event.getSource())).getScene().getWindow();
            menu.close();
        } else {
            //ir pagina anterior

            Doctor.setDisable(false);
            Pharmacy.setDisable(false);
            Patient.setDisable(false);
            label_pass.setVisible(false);
            label_user.setVisible(false);

            show_common_register();
            text1.setText(login.user.getName());
            text2.setText(login.getUsername());
            text6.setText(login.user.getEmail());

            text5.setText(login.user.getPhone());
            next_window = false;

        }
    }

    public void phone_check(KeyEvent event) {

        if (!next_window) {
            if (!((int) event.getCharacter().charAt(0) >= 48 && (int) event.getCharacter().charAt(0) < 58) && (int) event.getCharacter().charAt(0) != 8) {
                text5.deletePreviousChar();
            }
            if (text5.getText().length() >= 9) {
                phone_chek = true;
                label_phone.setVisible(false);
            } else {
                phone_chek = false;
                label_phone.setVisible(true);
            }
        } else {
            if ((int) event.getCharacter().charAt(0) >= 48 && (int) event.getCharacter().charAt(0) < 58) {
                text5.deletePreviousChar();
            }


        }
    }

    public void email_check(KeyEvent event) {

        if ((int) event.getCharacter().charAt(0) == 32) text6.deletePreviousChar();

        if (email_bool == true && (int) event.getCharacter().charAt(0) == 64) text6.deletePreviousChar();

        if (text6.getText().contains("@")) {
            email_bool = true;
        } else email_bool = false;

    }

    public void health_number_check(KeyEvent event) throws SQLException {

        if (Patient.isSelected() && !((int) event.getCharacter().charAt(0) >= 48 && (int) event.getCharacter().charAt(0) < 58) && (int) event.getCharacter().charAt(0) != 8) {
            text3.deletePreviousChar();
            label_pass.setVisible(false);
        }
        if(event.getCharacter().charAt(0) == 8) return;
        if (Patient.isSelected() && Data_base.procura_id_unico(Integer.parseInt(text3.getText().toString()), "Patient")) {
            label_pass.setText("Already in the system");
            label_pass.setVisible(true);
            health_number_bool = false;

        } else {
            label_pass.setVisible(false);
            if (!text3.getText().isBlank()) {
                health_number_bool = true;
            }
        }

    }

    @FXML
    void date_pick(ActionEvent event) {
        if (!data.getValue().isBefore(LocalDate.now())) {
            data.getEditor().clear();
        }

    }

}
