package Actions;

import javafx.scene.control.Button;

import javax.xml.stream.events.Comment;
import java.time.LocalDate;
import java.util.Date;

 public class Prescription {

    private int Code;
    private String Dose;
    private String Duration; //horas entre tomas
    private int Quantity;
    private LocalDate Expiration_date;  // duracao da receita; 1 mes pra simples e x pra periodica
    private String Comment;
    private String Drug;
    private String Doctor;
    private String Patient;
     private String Type;
     private String Active_subs;

    public Prescription(int Code,String Dose, String Duration, int Quantity,LocalDate Expiration_date,String Comment,String Drug,String Doctor,String Patient,String Type, String Active_subs){
        /*
         * Appointment constructor where a new prescription object is created
         * @param Code: String - prescription ID
         * @param Dose: String - how many pills to take
         * @param Duration: String - how many days to take
         * @param Quantity: int - quantity of the medicines
         * @param Expiration_date: LocalDate - expiration date to get the prescription
         * @param Comment: String - comment that doctor finds relevant
         * @param Drug: String - Name of the medicine used
         * @param Doctor: String - Hour of the doctor
         * @param Patient: String - Name of the patient
         * @param Type: String - type of prescription - automatic ou manual (made by doctor)
         * @param Active_subs: String - active substance
         */
        this.Code=Code;
        this.Dose=Dose;
        this.Duration=Duration;
        this.Quantity=Quantity;
        this.Expiration_date=Expiration_date;
        this.Comment=Comment;
        this.Drug=Drug;
        this.Patient=Patient;
        this.Doctor=Doctor;
        this.Type=Type;
        this.Active_subs = Active_subs;
    }

    public int getCode(){
        /*
         * Get the precription code to be identified
         * @return The prescription Code (identifier)
         */
        return Code;
    }
    public String getDose(){
        return Dose;
    }
    public String getDuration(){
        return Duration;
    }
    public int getQuantity(){
        return Quantity;
    }
    public LocalDate getExpiration_date(){
        return Expiration_date;
    }
    public String getDrug(){
        return Drug;
    }
     public String getComment(){
         return Comment;
     }
     public String getDoctor(){
         return Doctor;
     }
     public String getPatient(){
         return Patient;
     }

     public String getType(){
         return Type;
     }
     public String getActive_subs(){
         return Active_subs;
     }


    protected void setCode(int Code){  // nao muda, só na inicializacao da prescricao simples/periodica
        this.Code=Code;
    }
     public void setDose(String Dose){
        this.Dose=Dose;
    }
    public void setDuration(String Duration){
        this.Duration=Duration;
    }
    public void setQuantity(int Quantity){
        this.Quantity = Quantity;
    }
    public void setExpiration_date(LocalDate Expiration_date){
        this.Expiration_date=Expiration_date;
    }
    public void setMedicine(String Drug){
        this.Drug=Drug;
    }
     public void setComment(String Comment){
         this.Comment=Comment;
     }
     public void setDoctor(String Doctor){
         this.Doctor=Doctor;
     }
     public void setPatient(String Patient){
         this.Patient=Patient;
     }
     public void setType(String Type){
         this.Type=Type;
     }
     public void setActive_subs(String Active_subs){
         this.Active_subs=Active_subs;
     }


}
