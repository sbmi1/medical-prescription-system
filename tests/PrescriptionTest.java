import Actions.Prescription;
import org.junit.Test;
import java.time.LocalDate;
import static org.junit.Assert.*;

public class PrescriptionTest {

    @Test
    public void getExpiration_date() {
        int code = 1;
        String Dose = "2 pills";
        String Dur = "3 days";
        int quant = 2;
        LocalDate exp = LocalDate.now();
        String comment = "with water";
        String drug = "med";
        String doc = "dr_test";
        String pat = "user";
        String Type = "1 time";
        String Act_sub = "medicines";

        Prescription presc = new Prescription(code, Dose, Dur, quant, exp, comment, drug, doc, pat, Type, Act_sub);

        assertEquals(LocalDate.now(), presc.getExpiration_date());
    }

    @Test
    public void getDoctor() {
        int code = 1;
        String Dose = "2 pills";
        String Dur = "3 days";
        int quant = 2;
        LocalDate exp = LocalDate.now();
        String comment = "with water";
        String drug = "med";
        String doc = "dr_test";
        String pat = "user";
        String Type = "1 time";
        String Act_sub = "medicines";

        Prescription presc = new Prescription(code, Dose, Dur, quant, exp, comment, drug, doc, pat, Type, Act_sub);

        assertEquals("dr_test", presc.getDoctor());
    }

    @Test
    public void getPatient() {
        int code = 1;
        String Dose = "2 pills";
        String Dur = "3 days";
        int quant = 2;
        LocalDate exp = LocalDate.now();
        String comment = "with water";
        String drug = "med";
        String doc = "dr_test";
        String pat = "user";
        String Type = "1 time";
        String Act_sub = "medicines";

        Prescription presc = new Prescription(code, Dose, Dur, quant, exp, comment, drug, doc, pat, Type, Act_sub);

        assertEquals("user", presc.getPatient());
    }

    @Test
    public void setPatient() {
        int code = 1;
        String Dose = "2 pills";
        String Dur = "3 days";
        int quant = 2;
        LocalDate exp = LocalDate.now();
        String comment = "with water";
        String drug = "med";
        String doc = "dr_test";
        String pat = "user";
        String Type = "1 time";
        String Act_sub = "medicines";

        Prescription presc = new Prescription(code, Dose, Dur, quant, exp, comment, drug, doc, pat, Type, Act_sub);

        presc.setPatient("tester");
        assertNotEquals("user", presc.getPatient());
        assertEquals("tester", presc.getPatient());
    }
}