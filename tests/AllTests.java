import Actions.Prescription;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({AppointmentTest.class, PrescriptionTest.class})

public class AllTests {

}
